# AutomaTex

## Description du projet

**AutomaTex** est un projet éducatif visant à partager des exercices de type "automatismes" entièrement codés en LaTeX, sous forme de présentations Beamer. Ces exercices sont conçus pour générer aléatoirement des données sur chaque thème, permettant ainsi de proposer des exercices variés et adaptés aux élèves à chaque lancement.

### Objectif pédagogique

L'objectif principal est de renforcer les compétences des élèves à travers des séries d'exercices répétitifs, mais adaptés, qui couvrent divers thèmes du programme de mathématiques en collège. Grâce à la génération aléatoire des données, chaque élève peut travailler sur des exercices uniques tout en étant exposé aux mêmes concepts fondamentaux.

## Exemple de Thèmes - Abordés dans AutomaTex 1 :

- **Calcul mental :** tables de multiplication et compléments à 100
- **Proportionnalité :** par retour à l'unité
- **Géométrie :** compter le nombre de carreaux dans un rectangle (→ introduction à la notion d'aire)

## Fonctionnalités

- Génération aléatoire des données pour chaque exercice, garantissant une diversité des questions.
- Code LaTeX complet et personnalisable pour les enseignants.
- Présentation des exercices sous forme de diapositives Beamer.
- Exercices prêts à l'emploi pour la salle de classe ou l'apprentissage à domicile.

## Comment contribuer ?

Les contributions sont les bienvenues ! Si vous souhaitez :
- Ajouter de nouveaux thèmes
- Améliorer les algorithmes de génération aléatoire
- Partager vos propres versions d'exercices

N'hésitez pas à proposer des modifications via des pull requests.

## Utilisation

Pour utiliser **AutomaTex** :
1. vous devez avoir installé LaTeX sur votre machine ou utiliser un compilateur en ligne comme [EduTex](https://www.edutex.ensciences.fr/) ou [Overleaf](https://fr.overleaf.com/).
2. Vous copiez-collez le code.
3. Vous compilez
4. Et c'est fini !

## Contribution

### Rédiger des tickets

Vous voyez des coquilles, vous avez des idées d'automatisme à proposer ? Vous pouvez vous connecter à La Forge des Communs Numérique et rédiger un ticket [→ici←](https://forge.apps.education.fr/lmdbt/automatex/-/issues).

### Vous souhaitez intégrer l'équipe ?

Vous pouvez essayer de nous contacter directement en rédigeant un ticket [→ici←]().

**Envie d'essayer avant de nous contacter ?**
Un exemple minimal de code est proposé [→là←](https://forge.apps.education.fr/lmdbt/automatex/-/blob/main/PourCommencer/UnePetiteSomme.tex) pour commencer à rédiger ses premiers atomes d'automatisme.

## Licence et auteurs

### Licence

Ce projet est sous licence **[CC-BY](https://creativecommons.org/licenses/by/4.0/)**. Vous êtes libre de partager et d'adapter ce projet à des fins personnelles et commerciales à condition de citer `AutomaTex`.

### Auteurs

- [Thomas Mounier](https://forge.apps.education.fr/tmounier) - Niveau Lycée
- [Cyril Iaconelli](https://forge.apps.education.fr/iaconellicyril) - Niveau collège

---

Fait avec ❤️ pour promouvoir l'apprentissage des mathématiques en libre accès !
